<?php
class Af_Zz_Proxy extends Plugin {
	private $host;

	function about() {
		return array(1.0,
			"Load external content via built-in proxy",
			"fox & Exagone313");
	}

	function is_public_method($method) {
		return $method === "imgproxy";
	}

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_RENDER_ARTICLE, $this);
		$host->add_hook($host::HOOK_RENDER_ARTICLE_CDM, $this);
		//$host->add_hook($host::HOOK_RENDER_ARTICLE_API, $this);
		$host->add_hook($host::HOOK_ENCLOSURE_ENTRY, $this);
		$host->add_hook($host::HOOK_SANITIZE, $this);
	}

    function hook_enclosure_entry($enc, $article_id, $rv) {
        $path = parse_url($enc["content_url"], PHP_URL_PATH);

        if (preg_match("/image/", $enc["content_type"]) || preg_match("/\.(jpe?g|png|gif|bmp)$/i", $path)) {
			$enc["content_url"] = $this->rewrite_url_if_needed($enc["content_url"], 0);
		}

		return $enc;
	}

	function hook_render_article($article) {
		return $this->hook_render_article_cdm($article);
	}

	/*function hook_render_article_api($headline) {
		return $this->hook_render_article_cdm($headline["headline"], true);
    }*/

	public function imgproxy() {

		$url = rewrite_relative_url(Config::get(Config::SELF_URL_PATH), $_REQUEST["url"]);
		$kind = (int) $_REQUEST["kind"]; // 1 = video

		// called without user context, let's just redirect to original URL
		if (!$_SESSION["uid"]) {
			header("Location: $url");
			return;
		}

		$extension = $kind == 1 ? '.mp4' : '.png';
		$local_filename = Config::get(Config::CACHE_DIR) . "/images/" . sha1($url) . $extension;

		header("Content-Disposition: inline; filename=\"".basename($local_filename)."\"");

		if (file_exists($local_filename)) {
			$mimetype = mime_content_type($local_filename);
			header("Content-type: $mimetype");

			$stamp = gmdate("D, d M Y H:i:s", filemtime($local_filename)). " GMT";
			header("Last-Modified: $stamp", true);

			readfile($local_filename);
		} else {
			$data = fetch_file_contents(array("url" => $url, "useragent" => "Mozilla/5.0"));

			if ($data) {
				if (file_put_contents($local_filename, $data)) {
					$mimetype = mime_content_type($local_filename);
					header("Content-type: $mimetype");
				}

				print $data;
			}
		}
	}

    function rewrite_url_if_needed($url, $kind) {
		$scheme = parse_url($url, PHP_URL_SCHEME);
        $host = parse_url($url, PHP_URL_HOST);
        $self_host = parse_url(Config::get(Config::SELF_URL_PATH), PHP_URL_HOST);

		if ($host != $self_host) {
			if (strpos($url, "data:") !== 0) {
				$url = "public.php?op=pluginhandler&plugin=af_zz_proxy&pmethod=imgproxy&kind=$kind&url=" .
					urlencode($url);
			}
		}

		return $url;
	}

	function hook_render_article_cdm($article, $api_mode = false) {
		if (!$article["content"]) {
			return $article;
		}

		$need_saving = false;

		$doc = new DOMDocument();
		if (@$doc->loadHTML($article["content"])) {
			$xpath = new DOMXPath($doc);
			$imgs = $xpath->query("//img[@src]");

			foreach ($imgs as $img) {
				$new_src = $this->rewrite_url_if_needed($img->getAttribute("src"), 0);

				if ($new_src != $img->getAttribute("src")) {
					$img->setAttribute("src", $new_src);

					$need_saving = true;
				}
			}

			$vids = $xpath->query("//video");

			foreach ($vids as $vid) {
				if ($vid->hasAttribute("poster")) {
					$new_src = $this->rewrite_url_if_needed($vid->getAttribute("poster"), 0);

					if ($new_src != $vid->getAttribute("poster")) {
						$vid->setAttribute("poster", $new_src);

						$need_saving = true;
					}
				}

				$vsrcs = $xpath->query("source", $vid);

				foreach ($vsrcs as $vsrc) {
					$new_src = $this->rewrite_url_if_needed($vsrc->getAttribute("src"), 1);

					if ($new_src != $vsrc->getAttribute("src")) {
						$vid->setAttribute("src", $new_src);

						$need_saving = true;
					}
				}
			}
		}

		if ($need_saving) $article["content"] = $doc->saveXML();

		return $article;
	}

	function hook_sanitize($doc, $site_url, $allowed_elements, $disallowed_attributes, $article_id) { // TODO get images for videos (with setting?)

		$xpath = new DOMXpath($doc);
		$entries = $xpath->query('//iframe');

        foreach ($entries as $entry) {
            if($entry->hasAttribute('src')) {
                $iframe_url = $entry->getAttribute('src');
                $iframe_domain = parse_url($iframe_url, PHP_URL_HOST);
                if($iframe_domain !== null && $iframe_domain !== false) { // prevent relative link
                    $div = $doc->createElement('div');
                    $p = $doc->createElement('p');
                    $link = $doc->createElement('a');
                    $link->setAttribute('href', $iframe_url);
                    $link->setAttribute('target', '_blank');
                    $link->setAttribute('rel', 'noopener noreferrer');
                    $hasimg = false;
                    if($iframe_domain === 'www.youtube.com' || $iframe_domain === 'www.youtube-nocookie.com') {
                        $path = parse_url($iframe_url, PHP_URL_PATH);
                        if($path !== null && $path !== false) {
                            error_log('yt path: '. $path);
                            $pos = strrpos($path, '/');
                            $ytid = substr($path, $pos + 1);
                            if($ytid) {
                                $link->setAttribute('href', 'https://www.youtube.com/watch?v=' . $ytid);
                                $img = $doc->createElement('img');
                                $img->setAttribute('src', 'https://img.youtube.com/vi/' . $ytid . '/0.jpg'); // will be proxied
                                $img->setAttribute('alt', 'Youtube ' . $ytid);
                                $img->setAttribute('title', 'Youtube video');
                                $link->appendChild($img);
                                $hasimg = true;
                            }
                        }
                    }
                    if(!$hasimg) {
                        $link->nodeValue = '[iframe to ' . $iframe_domain . ']';
                    }
                    $p->appendChild($link);
                    $div->appendChild($p);
                    $entry->parentNode->insertBefore($div,$entry);
                }
            }
            $entry->parentNode->removeChild($entry);
		}

		return array($doc, $allowed_elements, $disallowed_attributes);
	}

	function api_version() {
		return 2;
	}
}
