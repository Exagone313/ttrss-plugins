<?php
class Utm_Clean extends Plugin {
	private $host;

	function about() {
		return array(1.0,
			'Remove utm parameters from article links',
			'Exagone313 + af_unburn code by fox');
	}

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
	}

	function hook_article_filter($article) {
		$real_url = $article['link'];
		$query = parse_url($real_url, PHP_URL_QUERY);

		if ($query && (strpos($query, 'utm_source') !== false
				|| strpos($query, 'utm_medium') !== false
				|| strpos($query, 'utm_campaign') !== false)) {
			$args = [];
			parse_str($query, $args);

			foreach (['utm_source', 'utm_medium', 'utm_campaign'] as $param) {
				if (array_key_exists($param, $args)) {
					unset($args[$param]);
				}
			}

			$new_query = http_build_query($args);

			if ($new_query !== $query) {
				$real_url = str_replace('?' . $query, '?' . $new_query, $real_url);
			}
		}

        $real_url = preg_replace('/\?$/', '', $real_url);

		$fragment = parse_url($real_url, PHP_URL_FRAGMENT);

		if ($fragment && (strpos($fragment, 'utm_source') !== false
				|| strpos($fragment, 'utm_medium') !== false
				|| strpos($fragment, 'utm_campaign') !== false)) {
			$real_url = str_replace('#' . $fragment, '', $real_url);
		}

		$article['link'] = $real_url;
		return $article;
	}

	function api_version() {
		return 2;
	}

}
